from lims import Project, Sample, Lab
from autocomplete_light import registry
registry.register(Project)
registry.register(Sample)
registry.register(Lab)
